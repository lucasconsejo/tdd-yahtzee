﻿namespace Yahtzee.Enum
{
    public enum Combinaisons
    {
        Ones = 1,
        Twos = 2,
        Threes = 3,
        Fours = 4,
        Fives = 5,
        Sixes = 6,
        ThreeOfKind = 7,
        FourOfAKind = 8,
        SmallStraight = 9,
        LargeStraight = 10,
        Yahtzee = 11,
        Chance = 12,
        Sum = 13,
        Bonus = 14,
        FullHouse = 15,
    }
}