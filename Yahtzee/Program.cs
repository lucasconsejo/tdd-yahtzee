﻿using System;
using Yahtzee.Service;

namespace Yahtzee
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();
            Player player = new Player();
            var result = player.RollDices(5);
            string printRoll = string.Empty;
            foreach(int nb in result)
            {
                printRoll += $"{nb}, "; 
            }
            Console.WriteLine($"Roll: {printRoll}");
            //Console.WriteLine($"Ones: {game.CalculateOnes(result)}");
            //Console.WriteLine($"Twos: {game.CalculateTwos(result)}");
            //Console.WriteLine($"Threes: {game.CalculateThrees(result)}");
            //Console.WriteLine($"Fours: {game.CalculateFours(result)}");
            //Console.WriteLine($"Fives: {game.CalculateFives(result)}");
            //Console.WriteLine($"Sixes: {game.CalculateSixes(result)}");
            //Console.WriteLine($"Three of a kind: {game.CalculateThreeOfAKind(result)}");
            //Console.WriteLine($"Four of a kind: {game.CalculateFourOfAKind(result)}");
            //Console.WriteLine($"Full house: {game.CalculateFullHouse(result)}");
            //Console.WriteLine($"Small straight: {game.CalculateSmallStraight(result)}");
            //Console.WriteLine($"Large straight: {game.CalculateLargeStraight(result)}");
            //Console.WriteLine($"Chance: {game.CalculateChance(result)}");
            //Console.WriteLine($"Yahtzee: {game.CalculateYahtzee(result)}");
        }
    }
}
