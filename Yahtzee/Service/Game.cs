﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Yahtzee.Enum;

namespace Yahtzee.Service
{
    public class Game
    {
        public int CalculateByCombinaison(Combinaisons combinaisons, List<int> dicesValues)
        {
            switch (combinaisons)
            {
                case Combinaisons.ThreeOfKind:
                    return CalculateThreeOfAKind(dicesValues);
                case Combinaisons.FourOfAKind:
                    return CalculateFourOfAKind(dicesValues);
                case Combinaisons.SmallStraight:
                    return CalculateSmallStraight(dicesValues);
                case Combinaisons.LargeStraight:
                    return CalculateLargeStraight(dicesValues);
                case Combinaisons.Yahtzee:
                    return CalculateYahtzee(dicesValues);
                case Combinaisons.FullHouse:
                    return CalculateFullHouse(dicesValues);
                case Combinaisons.Sum:
                    return CalculateSum(new List<int> { });
                case Combinaisons.Chance:
                    return CalculateChance(dicesValues);
                default:
                    return CalculateNumberCombinaison(dicesValues, combinaisons);
            }
        }
        public void Calculate()
        {

            List<int> roll = new Player().RollDices(5);
            List<int> totalResult = new List<int>();

            totalResult.Add(CalculateByCombinaison(Combinaisons.Ones, roll));
            totalResult.Add(CalculateByCombinaison(Combinaisons.Twos, roll));
            totalResult.Add(CalculateByCombinaison(Combinaisons.Threes, roll));
            totalResult.Add(CalculateByCombinaison(Combinaisons.Fours, roll));
            totalResult.Add(CalculateByCombinaison(Combinaisons.Fives, roll));
            totalResult.Add(CalculateByCombinaison(Combinaisons.Sixes, roll));
            totalResult.Add(CalculateByCombinaison(Combinaisons.SmallStraight, roll));
            totalResult.Add(CalculateByCombinaison(Combinaisons.LargeStraight, roll));
            totalResult.Add(CalculateByCombinaison(Combinaisons.FullHouse, roll));
            totalResult.Add(CalculateByCombinaison(Combinaisons.Yahtzee, roll));
            totalResult.Add(CalculateByCombinaison(Combinaisons.Chance, roll));
            totalResult.Add(CalculateByCombinaison(Combinaisons.ThreeOfKind, roll));
            totalResult.Add(CalculateByCombinaison(Combinaisons.FourOfAKind, roll));

            List<int> sumRange = totalResult.GetRange(0, 5);
            var sum = CalculateSum(sumRange);
            var bonus = CalculateBonus(sum);

            totalResult.Add(sum);
            totalResult.Add(bonus);

            CalculateTotal(totalResult);
        } 


        private int CalculateNumberCombinaison(List<int> dicesValues, Combinaisons combinaison)
        {
            return dicesValues.FindAll(r => r == ((int)combinaison)).Sum();
        }
        private int CalculateThreeOfAKind(List<int> diceValues)
        {
            var groups = diceValues.GroupBy(v => v);
            foreach (var group in groups)
            {
                if (group.Count() >= 3)
                {
                    return diceValues.Sum();
                }
            }
            return 0;
        }
        private int CalculateFourOfAKind(List<int> diceValues)
        {
            var groups = diceValues.GroupBy(v => v);
            foreach (var group in groups)
            {
                if (group.Count() >= 4)
                {
                    return diceValues.Sum();
                }
            }
            return 0;
        }
        private int CalculateYahtzee(List<int> diceValues)
        {
            var groups = diceValues.GroupBy(v => v);
            foreach (var group in groups)
            {
                if (group.Count() == 5)
                {
                    return 50;
                }
            }
            return 0;
        }
        private int CalculateFullHouse(List<int> diceValues)
        {
            var groups = diceValues.GroupBy(v => v);
            if (groups.Count() == 2)
            {
                if (groups.ElementAt(0).Count() == 3 && groups.ElementAt(1).Count() == 2)
                {
                    return 25;
                }
                if (groups.ElementAt(0).Count() == 2 && groups.ElementAt(1).Count() == 3)
                {
                    return 25;
                }
            }
            return 0;
        }
        private int CalculateSmallStraight(List<int> diceValues)
        {
            if (diceValues.Contains(1) && diceValues.Contains(2) && diceValues.Contains(3) && diceValues.Contains(4))
            {
                return 30;
            }
            if (diceValues.Contains(2) && diceValues.Contains(3) && diceValues.Contains(4) && diceValues.Contains(5))
            {
                return 30;
            }
            if (diceValues.Contains(3) && diceValues.Contains(4) && diceValues.Contains(5) && diceValues.Contains(6))
            {
                return 30;
            }
            return 0;
        }
        private int CalculateLargeStraight(List<int> diceValues)
        {
            if (diceValues.Contains(1) && diceValues.Contains(2) && diceValues.Contains(3) && diceValues.Contains(4) && diceValues.Contains(5))
            {
                return 40;
            }
            if (diceValues.Contains(2) && diceValues.Contains(3) && diceValues.Contains(4) && diceValues.Contains(5) && diceValues.Contains(6))
            {
                return 40;
            }
            return 0;
        }
        private int CalculateScore(int typeScore, List<int> diceValues)
        {
            return diceValues.FindAll(r => r == typeScore).Sum();
        }
        private int CalculateChance(List<int> dicesValues)
        {
            return dicesValues.Sum();
        }
        private int CalculateSum(List<int> results)
        {
            if (results.Count() == 6)
            {
                return results.Sum();
            }
            return 0;
        }
        private int CalculateBonus(int sumResult)
        {
            if (sumResult >= 63)
            {
                return 35;
            }
            return 0;
        }
        private int CalculateTotal(List<int> totalResult)
        {
            if (totalResult.Count() == 15)
            {
                return totalResult.Sum();
            }
            return 0;
        }
    }
}