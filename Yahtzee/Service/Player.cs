﻿using System.Collections.Generic;

namespace Yahtzee.Service
{
    public class Player
    {
        public List<int> RollDices(int nbDice)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < nbDice; i++)
            {
                Dice dice = new Dice();
                result.Add(dice.Roll());
            }
            return result;
        }

    }
}