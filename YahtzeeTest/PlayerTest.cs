﻿using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Yahtzee.Service;

namespace YahtzeeTest
{
    public class PlayerTest
    {
        [Fact]
        public void TestPlayerRoll_ReturnArrayOneScore()
        {
            Player player = new Player();
            var result = player.RollDices(1);
            Assert.Single(result);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(5)]
        public void TestPlayerRoll_ReturnArrayScore(int nbDice)
        {
            Player player = new Player();
            var result = player.RollDices(nbDice);
            Assert.Equal(nbDice, result.Count);
        }

    }
}
