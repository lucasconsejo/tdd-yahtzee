﻿using System.Collections.Generic;
using System.Linq;
using Xunit;
using Yahtzee.Enum;
using Yahtzee.Service;

namespace YahtzeeTest
{
    public class GameTest
    {
        [Theory]
        [InlineData(new int[] { 2, 3, 3, 4, 5 }, 0)]
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 1)]
        [InlineData(new int[] { 1, 1, 1, 1, 1 }, 5)]
        public void TestGame_CalculateOnes(int[] dicesValue, int expectedScore)
        {
            Game game = new Game();
            var expected = dicesValue.OfType<int>().ToList();

            var result = game.CalculateByCombinaison(Combinaisons.Ones, expected); ;

            Assert.Equal(expectedScore, result);
        }

        [Theory]
        [InlineData(new int[] { 1, 3, 3, 4, 5 }, 0)]
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 2)]
        [InlineData(new int[] { 2, 2, 2, 2, 2 }, 10)]
        public void TestGame_CalculateTwos(int[] dicesValue, int expectedScore)
        {
            Game game = new Game();
            var expected = dicesValue.OfType<int>().ToList();

            var result = game.CalculateByCombinaison(Combinaisons.Twos, expected); ;

            Assert.Equal(expectedScore, result);
        }

        [Theory]
        [InlineData(new int[] { 1, 2, 2, 4, 5 }, 0)]
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 3)]
        [InlineData(new int[] { 3, 3, 3, 3, 3 }, 15)]
        public void TestGame_CalculateThrees(int[] dicesValue, int expectedScore)
        {
            Game game = new Game();
            var expected = dicesValue.OfType<int>().ToList();

            var result = game.CalculateByCombinaison(Combinaisons.Threes, expected); ;

            Assert.Equal(expectedScore, result);
        }

        [Theory]
        [InlineData(new int[] { 1, 2, 2, 2, 5 }, 0)]
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 4)]
        [InlineData(new int[] { 4, 4, 4, 4, 4 }, 20)]
        public void TestGame_CalculateFours(int[] dicesValue, int expectedScore)
        {
            Game game = new Game();
            var expected = dicesValue.OfType<int>().ToList();

            var result = game.CalculateByCombinaison(Combinaisons.Fours, expected); ;

            Assert.Equal(expectedScore, result);
        }

        [Theory]
        [InlineData(new int[] { 2, 3, 3, 4, 4 }, 0)]
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 5)]
        [InlineData(new int[] { 5, 5, 5, 5, 5 }, 25)]
        public void TestGame_CalculateFives(int[] dicesValue, int expectedScore)
        {
            Game game = new Game();
            var expected = dicesValue.OfType<int>().ToList();

            var result = game.CalculateByCombinaison(Combinaisons.Fives, expected); ;

            Assert.Equal(expectedScore, result);
        }

        [Theory]
        [InlineData(new int[] { 2, 3, 3, 4, 4 }, 0)]
        [InlineData(new int[] { 1, 2, 3, 4, 6 }, 6)]
        [InlineData(new int[] { 6, 6, 6, 6, 6 }, 30)]
        public void TestGame_CalculateSixes(int[] dicesValue, int expectedScore)
        {
            Game game = new Game();
            var expected = dicesValue.OfType<int>().ToList();

            var result = game.CalculateByCombinaison(Combinaisons.Sixes, expected); ;

            Assert.Equal(expectedScore, result);
        }

        [Theory]
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 0)]
        [InlineData(new int[] { 1, 1, 1, 1, 6 }, 10)]
        [InlineData(new int[] { 3, 3, 3, 4, 2 }, 15)]
        public void TestGame_CalculateThreeOfAKind(int[] dicesValue, int expectedScore)
        {
            Game game = new Game();
            var expected = dicesValue.OfType<int>().ToList();

            var result = game.CalculateByCombinaison(Combinaisons.ThreeOfKind, expected); ;

            Assert.Equal(expectedScore, result);
        }

        [Theory]
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 0)]
        [InlineData(new int[] { 1, 1, 1, 1, 6 }, 10)]
        [InlineData(new int[] { 1, 1, 1, 1, 1 }, 5)]
        [InlineData(new int[] { 3, 3, 3, 4, 2 }, 0)]
        public void TestGame_CalculateFourOfAKind(int[] dicesValue, int expectedScore)
        {
            Game game = new Game();
            var expected = dicesValue.OfType<int>().ToList();

            var result = game.CalculateByCombinaison(Combinaisons.FourOfAKind, expected); ;

            Assert.Equal(expectedScore, result);
        }

        [Theory]
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 0)]
        [InlineData(new int[] { 1, 1, 1, 1, 1 }, 50)]
        public void TestGame_CalculateYahtzee(int[] dicesValue, int expectedScore)
        {
            Game game = new Game();
            var expected = dicesValue.OfType<int>().ToList();

            var result = game.CalculateByCombinaison(Combinaisons.Yahtzee, expected); ;

            Assert.Equal(expectedScore, result);
        }

        [Theory]
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 0)]
        [InlineData(new int[] { 3, 3, 3, 1, 4 }, 0)]
        [InlineData(new int[] { 3, 3, 2, 1, 4 }, 0)]
        [InlineData(new int[] { 1, 3, 3, 1, 1 }, 25)]
        [InlineData(new int[] { 1, 1, 1, 2, 2 }, 25)]
        public void TestGame_CalculateFullHouse(int[] dicesValue, int expectedScore)
        {
            Game game = new Game();
            var expected = dicesValue.OfType<int>().ToList();

            var result = game.CalculateByCombinaison(Combinaisons.FullHouse, expected); ;

            Assert.Equal(expectedScore, result);
        }

        [Theory]
        [InlineData(new int[] { 6, 6, 3, 4, 2 }, 0)]
        [InlineData(new int[] { 1, 2, 3, 4, 2 }, 30)]
        [InlineData(new int[] { 2, 3, 4, 5, 3 }, 30)]
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 30)]
        [InlineData(new int[] { 2, 1, 3, 4, 5 }, 30)]
        public void TestGame_CalculateSmallStraight(int[] dicesValue, int expectedScore)
        {
            Game game = new Game();
            var expected = dicesValue.OfType<int>().ToList();

            var result = game.CalculateByCombinaison(Combinaisons.SmallStraight, expected);

            Assert.Equal(expectedScore, result);
        }

        [Theory]
        [InlineData(new int[] { 6, 6, 3, 4, 2 }, 0)]
        [InlineData(new int[] { 1, 2, 3, 4, 2 }, 0)]
        [InlineData(new int[] { 1, 2, 3, 4, 5 }, 40)]
        [InlineData(new int[] { 2, 3, 4, 5, 6 }, 40)]
        [InlineData(new int[] { 2, 4, 3, 5, 1 }, 40)]
        public void TestGame_CalculateLargeStraight(int[] dicesValue, int expectedScore)
        {
            Game game = new Game();
            var expected = dicesValue.OfType<int>().ToList();

            var result = game.CalculateByCombinaison(Combinaisons.LargeStraight, expected);

            Assert.Equal(expectedScore, result);
        }

        [Fact]
        public void TestGame_CalculateChance()
        {
            Game game = new Game();

            var result = game.CalculateByCombinaison(Combinaisons.Chance, new List<int> { 1, 2, 2, 3, 4 });

            Assert.Equal(12, result);
        }

        //[Theory]
        //[InlineData(new int[] { 2 }, 0)]
        //[InlineData(new int[] { 2, 4, 6, 8 }, 0)]
        //[InlineData(new int[] { 2, 4, 6, 8, 10, 12 }, 42)]
        //public void TestGame_CalculateSum(int[] dicesResults, int expectedScore)
        //{
        //    Game game = new Game();
        //    var expected = dicesResults.OfType<int>().ToList();

        //    var result = game.Calculate(Combinaisons.Sum, expected);

        //    Assert.Equal(expectedScore, result);
        //}

        //[Theory]
        //[InlineData(1, 0)]
        //[InlineData(62, 0)]
        //[InlineData(63, 35)]
        //[InlineData(64, 35)]
        //public void TestGame_CalculateBonus(int sumResult, int expectedScore)
        //{
        //    Game game = new Game();

        //    var result = game.Calculate(Combinaisons.Bonus, su);

        //    Assert.Equal(expectedScore, result);
        //}

        //[Theory]
        //[InlineData(new int[] { 2 }, 0)]
        //[InlineData(new int[] { 2, 4, 6, 8 }, 0)]
        //[InlineData(new int[] { 2, 4, 6, 8, 10, 12, 2, 4, 6, 8, 10, 12, 4, 8, 11 }, 107)]
        //public void TestGame_CalculateTotal(int[] dicesResults, int expectedScore)
        //{
        //    Game game = new Game();
        //    var expected = dicesResults.OfType<int>().ToList();

        //    var result = game.CalculateTotal(expected);

        //    Assert.Equal(expectedScore, result);
        //}
    }
}
