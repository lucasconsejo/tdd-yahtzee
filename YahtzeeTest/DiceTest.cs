﻿using System;
using Xunit;
using Yahtzee.Service;
using Moq;

namespace YahtzeeTest
{
    public class DiceTest
    {
        [Fact]
        public void TestRoll1Dice_ReturnRandom()
        {
            Dice dice = new Dice();
            var result = dice.Roll();
            Assert.InRange(result, 1, 6);
        }
    }
}
